# SETUP #

Add me to composer:

```
  "repositories": [
    {
      "url": "https://bitbucket.org/videodock/poc-word-find",
      "type": "git"
    }
```

```
  "require": {
     "qaribou/immutable.php": "dev-master”,
     "videodock/poc-word-find": "dev-master"
  }
```



# Example #

```php
//Find words in both directions and only return results with at least 4 characters
$sentence = 'boerzoektvrouw';

$wf = new WordFinder();
$wf->breakSentenceIntoWords($sentence, WordFinder::DIRECTION_AUTO)->getResults(4)
```

