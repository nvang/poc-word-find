<?php
require_once(__DIR__ . '/../vendor/autoload.php');

$sentence       = $argv[1];

use Videodock\Component\WordFinder\WordFinder;

$finder = new WordFinder();

$start      = microtime(true);
$results    = ($finder->breakSentenceIntoWords($sentence, WordFinder::DIRECTION_AUTO)->getResults());
$lookupTime = microtime(true) - $start;

print PHP_EOL;
print 'search: ' . $sentence . PHP_EOL;
print 'result: ' . implode(' ', $results) . PHP_EOL;
print 'lookup time: ' . $lookupTime . PHP_EOL;