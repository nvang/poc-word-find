<?php
require_once(__DIR__ . '/../vendor/autoload.php');

$srcDictionaryPath = $argv[1];

//convert ORI to TXT
$contents = file($srcDictionaryPath);

foreach ($contents as $word) {
    $word = trim($word);
    $word = strpos($word, '/') !== false ? strstr($word, '/', true) : $word;
    $word = str_replace(array('ĳ', 'Ĳ'), array('ij', 'Ij'), $word);

    if (empty($word)) {
        continue;
    }
    print trim($word) . PHP_EOL;
}

//some known words to add that are missing
print 'boer' . PHP_EOL;
print 'vrouw' . PHP_EOL;