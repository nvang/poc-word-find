<?php
namespace Videodock\Component\WordFinder\Helpers;

/**
 * ArrayHelper.php
 *
 * @author      Wim Klerkx <wim@videodock.com>
 * @copyright   2015 Video Dock b.v.
 *
 * This file is part of the poc-word-find project.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

class ArrayHelper
{

    /**
     * @param $array
     * @return array
     */
    public function uniqueCI(array $array)
    {
        $array = array_intersect_key(
            $array,
            array_unique(array_map("StrToLower", $array))
        );
        return $array;
    }

    /**
     * @param $array
     * @return mixed
     */
    public function alphanumericClean(array $array)
    {
        foreach ($array as $key => &$value) {
            $value = preg_replace("/[^a-zA-Z0-9]+/", "", $value);
            // remove if word is empty now
            if ($value === '') {
                unset($array[$key]);
            }
        }
        return $array;
    }

    /**
     * @param array $array
     * @param $minLength
     * @return array
     */
    public function valueMinLength(array $array, $minLength)
    {
        $minLength = (int)$minLength;
        $array = array_values(
            array_filter(
                $array,
                function ($value) use ($minLength) {
                    return strlen($value) >= $minLength;
                }
            )
        );
        return $array;
    }

}
