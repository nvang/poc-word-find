<?php
namespace Videodock\Component\WordFinder;

use Videodock\Component\WordFinder\Helpers\ArrayHelper;

/**
 * WordFinder.php
 *
 * @author      Nick van Ginkel <nick@videodock.com>
 * @copyright   2015 Video Dock b.v.
 *
 * This file is part of the wordfind project.
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

/**
 * Class WordFinder
 *
 * @package Videodock\Component\WordFinder
 */
class WordFinder
{
    /**
     *
     */
    const ORIGIN_CAPS = "caps";

    /**
     *
     */
    const ORIGIN_DICTIONARY = 'dictionary';

    /**
     *
     */
    const ORIGIN_ALL = 'all';

    /**
     *
     */
    const DIRECTION_FORWARD = 'forward';

    /**
     *
     */
    const DIRECTION_BACKWARD = 'backward';

    /**
     *
     */
    const DIRECTION_AUTO = 'auto';

    /**
     *
     */
    const RESULT_ORDER_NONE = 'none';

    /**
     *
     */
    const RESULT_ORDER_LENGTH = 'length';

    /**
     * @var array
     */
    private $dictionary;

    /**
     * @var array
     */
    private $results;

    /**
     * @var ArrayHelper
     */
    private $arrayHelper;

    /**
     * @var bool
     */
    protected $includeSplitOnCapsAsPhrase;

    /**
     * @param null $dictionaryPath
     */
    public function __construct($dictionaryPath = null)
    {
        if (!isset($dictionaryPath)) {
            $dictionaryPath = __DIR__ . '/Resources/dictionary/nl_NL.txt';
        }

        $this->loadDictionaryFromPath($dictionaryPath);
        $this->results                    = array();
        $this->arrayHelper                = new ArrayHelper();
        $this->includeSplitOnCapsAsPhrase = true;
    }

    /**
     * @param $path
     */
    private function loadDictionaryFromPath($path)
    {
        //cleanup words
        $contents = file($path);

        $this->dictionary = array_map(
            function ($word) {
                return trim($word);
            },
            $contents
        );
    }

    /**
     * @param $needle
     * @return string|null
     */
    private function getWordFromDictionary($needle)
    {
        $key = array_search($needle, $this->dictionary);
        return $key !== false ? $needle : null;
    }

    /**
     * @param $sentence
     * @param string $origin
     * @param string $direction
     * @return $this
     */
    public function split($sentence, $origin = self::ORIGIN_ALL, $direction = self::DIRECTION_FORWARD, $orderClause = self::RESULT_ORDER_NONE)
    {
        $length = strlen($sentence);

        if (is_string($sentence) && $length > 1) {

            // handle words deduced from camelCased segments
            if ($origin === self::ORIGIN_CAPS || $origin === self::ORIGIN_ALL) {
                $this->splitOnCaps($sentence);
            }

            // handle dictionary based words
            if ($origin === self::ORIGIN_DICTIONARY || $origin === self::ORIGIN_ALL) {
                $this->splitOnDictionary($sentence, $direction);
            }

            if($orderClause){
                $this->orderResults($orderClause);
            }

        }

        return $this;
    }

    /**
     * @param $sentence
     * @param string $direction
     * @return $this
     */
    public function splitOnDictionary($sentence, $direction = self::DIRECTION_FORWARD)
    {
        $length = strlen($sentence);

        if (is_string($sentence) && $length > 1) {
            switch ($direction) {
                case self::DIRECTION_FORWARD:
                    $this->breakSentenceIntoWordsForward($sentence);
                    break;

                case self::DIRECTION_BACKWARD:
                    $this->breakSentenceIntoWordsBackward($sentence);
                    break;

                case self::DIRECTION_AUTO:
                    // store any pre-existing results
                    $preservedResults  = $this->getResults();
                    $wordsForward = $this->breakSentenceIntoWordsForward($sentence)->getResults();
                    $this->resetResults();
                    $wordsBackward = $this->breakSentenceIntoWordsBackward($sentence)->getResults();

                    // restore previous results
                    $this->setResults($preservedResults);

                    //weak assumption smallest collection is better because it found less words (so longer words?)
                    $wordsToAdd = $wordsBackward;
                    if (count($wordsForward) < count($wordsBackward)) {
                        $wordsToAdd = $wordsForward;
                    }
                    // add either Forward or Backward results
                    foreach($wordsToAdd as $wordToAdd){
                        $this->addWord($wordToAdd);
                    }
                    break;
            }
        }

        return $this;
    }

    /**
     * @param $sentence
     * @return $this
     */
    private function breakSentenceIntoWordsForward($sentence)
    {
        $length = strlen($sentence);

        for ($i = $length; $i > 0; $i--) {
            $needle = substr($sentence, 0, $i);

            $result = $this->getWordFromDictionary($needle);

            if (isset($result)) {
                $this->addWord($needle);

                $newSentence = substr($sentence, $i);
                $this->breakSentenceIntoWordsForward($newSentence);
                break;
            }
        }

        return $this;
    }


    /**
     * @param $sentence
     * @return $this
     */
    private function breakSentenceIntoWordsBackward($sentence)
    {
        $length = strlen($sentence);

        for ($i = 0; $i < $length; $i++) {
            $needle = substr($sentence, $i, $length);
            $result = $this->getWordFromDictionary($needle);

            if (isset($result)) {
                $this->addWord($needle, false);
                $newSentence = substr($sentence, 0, $i);
                $this->breakSentenceIntoWordsBackward($newSentence);
                break;
            }
        }

        return $this;
    }

    /**
     * @param $sentence
     * @param int $minLength
     * @return $this
     */
    public function splitOnCaps($sentence, $minLength = 0)
    {
        // break sentence into the segments
        $segments = explode(' ', $sentence);

        // process each segment into words
        foreach ($segments as $segment) {
            $subresult = $this->camelcaseToWords($segment, $minLength);
            // add results first as concatenated phrase
            if ($this->includeSplitOnCapsAsPhrase && $subresult !== $segment) {
                $this->addWord(implode(' ', $subresult));
            }
            // then add each result to words
            if ($subresult) {
                foreach ($subresult as $word) {
                    $this->addWord($word);
                }
            }
        }

        return $this;
    }

    /**
     * @param $word
     * @param bool|true $push
     * @return $this
     */
    private function addWord($word, $push = true)
    {
        // word length check
        // TODO make class property for word minLength
        if (empty($word) || strlen($word) === 1) {
            return $this;
        }

        // determine which end to add
        if ($push) {
            $this->results[] = $word;
        } else {
            array_unshift($this->results, $word);
        }

        return $this;
    }

    /**
     * @param $string
     * @param int $minLength
     * @param bool|true $cleanAplhaNumeric
     * @return array|mixed
     */
    private function camelcaseToWords($string, $minLength = 0, $cleanAplhaNumeric = true)
    {
        // define regex (source: http://stackoverflow.com/questions/4519739/split-camelcase-word-into-words-with-php-preg-match-regular-expression)
        $re = '/(?#! splitCamelCase Rev:20140412)
        # Split camelCase "words". Two global alternatives. Either g1of2:
          (?<=[a-z])      # Position is after a lowercase,
          (?=[A-Z])       # and before an uppercase letter.
        | (?<=[A-Z])      # Or g2of2; Position is after uppercase,
          (?=[A-Z][a-z])  # and before upper-then-lower case.
        /x';

        // split
        $words = preg_split($re, $string);

        // default clean to alphanumeric results
        if (true === $cleanAplhaNumeric) {
            $words = $this->arrayHelper->alphanumericClean($words);
        }

        // optional min-length check on results
        if ($minLength && is_int($minLength)) {
            $words = $this->arrayHelper->valueMinLength($words, $minLength);
        }

        return $words;
    }

    /**
     * @param null $minLength
     * @param bool|true $uniqueValues
     * @return array
     */
    public function getResults($minLength = null, $uniqueValues = true)
    {
        $return = $this->results;

        // default convert to CI unique values
        if ($uniqueValues) {
            $return = $this->arrayHelper->uniqueCI($return);
        }

        // optional remove short values
        if (isset($minLength)) {
            $return = $this->arrayHelper->valueMinLength($return, $minLength);
        }

        // TODO optional order on length

        return $return;
    }

    /**
     *
     */
    private function resetResults()
    {
        $this->results = array();
    }

    /**
     * @param array $results
     */
    private function setResults(array $results)
    {
        $this->results = $results;
    }

    /**
     * @param string $orderOn
     */
    private function orderResults($orderOn = self::RESULT_ORDER_LENGTH)
    {
        switch($orderOn){
            case self::RESULT_ORDER_LENGTH:
                usort($this->results, function($a, $b) {
                    return strlen($b) - strlen($a);
                });
                break;
            case self::RESULT_ORDER_NONE:
                // do nothing
                break;
            default:
        }

    }

    /**
     * @return boolean
     */
    public function isIncludeSplitOnCapsAsPhrase()
    {
        return $this->includeSplitOnCapsAsPhrase;
    }

    /**
     * @param boolean $includeSplitOnCapsAsPhrase
     *
     * @return WordFinder
     */
    public function setIncludeSplitOnCapsAsPhrase($includeSplitOnCapsAsPhrase)
    {
        $this->includeSplitOnCapsAsPhrase = $includeSplitOnCapsAsPhrase;
        return $this;
    }

}
